FactoryGirl.define do
  factory :item1, :class => Item do |f|
    f.name 'Red'
    f.number 5
  end
  factory :item2, :class => Item do |f|
    f.name 'Green'
    f.number 2
  end
  factory :item3, :class => Item do |f|
    f.name 'Blue'
    f.number 7
  end
  factory :item4, :class => Item do |f|
    f.name 'Yellow'
    f.number 1
  end
  factory :item5, :class => Item do |f|
    f.name 'Gold'
    f.number 3
  end
end