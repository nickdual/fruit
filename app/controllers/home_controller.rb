class HomeController < ApplicationController
  def index
    @items = Item.limit(4).order('number desc')
    respond_to do |format|
      format.html { }
      format.json { render json: @items}
    end
  end

  def insert
    name = params["name"].downcase.titleize.strip
    item = Item.where(:name => name).first
    item.blank? ? item = Item.new(:name => name, :number => 1) : item.number += 1
    respond_to do |format|
      if item.save
        format.json { render json: {:status => "success", :data => Item.limit(4).order('number desc') }}
      else
        format.json { render json: {:status => "error", :data => item.errors}}
      end
    end
  end

  def list
    @items = Item.select('name, number').order('number desc')
    respond_to do |format|
      format.csv { send_data Item.to_csv(@items, [1,2]) }
    end
  end
end
